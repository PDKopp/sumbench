<?php

$GLOBALS['debug'] = false;

$GLOBALS['test'] = 1000000;

if (PHP_SAPI == 'cli' && $argc >= 2) {
	for($i = 1; $i < $argc; $i++) {
		if (is_numeric($argv[$i]))
			$GLOBALS['test'] = $argv[$i];
		if ($argv[$i] == 'debug')
			$GLOBALS['debug'] = true;
		if ($argv[$i] == 'help') {
			echo "This can be run with the command `php ".$argv[0]." [opt]`\n";
			echo "Where the options can be any of: \n";
			echo "- A number such as '2500000' to figure the sum of 2, 3 and 5 divisible numbers below\n";
			echo "- 'bench', where the timings will be calculated for 1,000,000 through 25,000,000 and stored in a CSV\n";
			echo "- The word 'debug' which will output some minor debugging information, proving concepts\n";
			echo "- 'help', which will show this help text\n";
			exit(0);
		}
		if ($argv[$i] == 'bench') {
			datatable(25);
			exit(0);
		}
	}
}

/**
 * uses global $test value, returns time taken in ms
 */
function naive() {
	$start = microtime(true);
	$sum = 0;
	for ($i = 0; $i < $GLOBALS['test']; $i++) {
		if (($i % 2 == 0) && ($i % 3 == 0) && ($i % 5 == 0))
			$sum += $i;
	}
	$end = microtime(true);
	if ($GLOBALS['debug'] && PHP_SAPI == 'cli')
		echo "Naive sum is: $sum\n";
	return number_format($end - $start, 14);
}

function lcm($step = 1) {
	$start = microtime(true);
	$sum = 0;
	for($i = 0; $i < $GLOBALS['test']; $i += $step) {
		if ($i % 30 == 0)
			$sum += $i;
	}
	$end = microtime(true);
	if ($GLOBALS['debug'] && PHP_SAPI == 'cli')
		echo "LCM sum is: $sum\n";
	return number_format($end - $start, 14);
}

function lcm_optimized() {
	$start = microtime(true);
	$sum = 0;
	for($i = 0; $i < $GLOBALS['test']; $i += 30) {
		$sum += $i;
	}
	$end = microtime(true);
	if ($GLOBALS['debug'] && PHP_SAPI == 'cli')
		echo "LCM Optimized sum is: $sum\n";
	return number_format($end - $start, 14);
}

function mathematical() {
	$start = microtime(true);
	$floor = floor($GLOBALS['test']/30);
	// http://www.regentsprep.org/regents/math/algtrig/atp2/arithseq.htm
	$sum = $floor * (30 + ($floor * 30)) / 2;
	$end = microtime(true);
	if ($GLOBALS['debug'] && PHP_SAPI == 'cli')
		echo "Mathematical sum is: $sum\n";
	return number_format($end - $start, 14);
}

function datatable($millions = 25) {
	$file = fopen('benchmarks.csv', 'w');
	$header = array('number','naive','lcm','lcm30','lcmopt','math');
	fputcsv($file, $header);
	for ($i = 1; $i <= $millions; $i++) {
		$GLOBALS['test'] = 1000000 * $i;
		$result = array($GLOBALS['test']);
		$result[] = naive();
		$result[] = lcm();
		$result[] = lcm(30);
		$result[] = lcm_optimized();
		$result[] = mathematical();
		fputcsv($file, $result);
		echo "Calculated for ".$GLOBALS['test']."\n";
	}
	echo "Output file generated\n";
}

if (PHP_SAPI == 'cli') {
	// CLI output
	echo "Naive time is         ".naive()." seconds\n";
	echo "LCM time is           ".lcm()." seconds\n";
	echo "LCM step 30 time is   ".lcm(30)." seconds\n";
	echo "LCM optimized time is ".lcm_optimized()." seconds\n";
	echo "Mathematical time is  ".mathematical()." seconds\n";
}
else {
	// Web browser output
	echo "<html>\n";
	echo "<head></head>\n";
	echo "<body>\n";
	echo "Naive time is ".naive()." seconds<br />\n";
	echo "LCM time is ".lcm()." seconds<br />\n";
	echo "LCM step 30 time is ".lcm(30)." seconds<br />\n";
	echo "LCM Optimized time is ".lcm_optimized()." seconds<br />\n";
	echo "Mathematical time is ".mathematical()." seconds<br />\n";
	echo "</body>\n</html>";
}

?>
